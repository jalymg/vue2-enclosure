import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const store = new Vuex.Store({
  // 定义状态
  state: {
    userInfo: {}, // 用户信息
    token: '', // token
  },
  //修改状态
  mutations: {
    //用户信息
    userInfo(state, msg) {
      state.userInfo = msg;
    },
    // 保存token
    setToken(state, token) {
      state.token = token;
      // 存入本地
      localStorage.setItem('l-mall-identity-client', token);
      // console.log('localStorage.getItem    ', localStorage.getItem('l-mall-identity-client'));
    },
  },
  plugins: [
    createPersistedState({
      storage: window.sessionStorage,
    }),
  ],
});

export default store;
