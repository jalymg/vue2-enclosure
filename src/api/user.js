import http from '@/utils/http.js';

/**
 * @description 登录
 * @author
 * @param {string} arg
 */
export const loginApi = data => {
  return http({
    url: '/login',
    method: 'post',
    data,
  });
};

/**
 * @description 注册
 * @author
 * @param {string} arg
 */
export const registerApi = data => {
  return http({
    url: '/add_user',
    method: 'post',
    data,
  });
};

/**
 * @description: 获取用户信息
 */
export const getUserInfoApi = data => {
  return http({
    url: '/user_info',
    method: 'post',
    data,
  });
};

/**
 * @description 更新用户信息
 * @author
 * @param {string} arg
 */
export const updateUserApi = data => {
  return http({
    url: '/update_user',
    method: 'post',
    data,
  });
};
