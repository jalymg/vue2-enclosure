export default {
  path: '/',
  component: () => import('@/layout/index'),
  redirect: '/home',
  children: [
    {
      path: 'home',
      name: 'MallHome',
      component: () => import('@/views/home'),
    },
  ],
};
