// 利用json的深拷贝
function deepCopyByJson(obj) {
  const objStr = JSON.stringify(obj);
  const copyObj = JSON.parse(objStr);
  return copyObj;
}

// 利用递归的深拷贝
function deepCopyByFor(obj) {
  let copyObj = null; // 接受拷贝的新对象
  // 判断是否是引用类型
  if (typeof obj == 'object' && obj) {
    copyObj = obj instanceof Array ? [] : {}; // 判断是数组还是对象
    for (let i in obj) {
      copyObj[i] = deepCopyByFor(obj[i]); // 判断下一级是否还是引用类型
    }
  } else {
    copyObj = obj;
  }
  return copyObj;
}

// 利用map的深拷贝 可解决循环引用
function deepCopyByMap(obj) {
  function copy(obj) {
    let copyObj = null;
    if (typeof obj == 'object' && obj) {
      if (map.get(obj)) {
        copyObj = map.get(obj); // 如果不想循环打印 可以设置为null
      } else {
        copyObj = obj instanceof Array ? [] : {}; // 判断是数组还是对象
        map.set(obj, copyObj);
        for (let i in obj) {
          copyObj[i] = copy(obj[i]); // 判断下一级是否还是引用类型
        }
      }
    } else {
      copyObj = obj;
    }
    return copyObj;
  }
  let map = new Map(); // Map
  return copy(obj);
}

// 获取时间 2022-04-02 11:54:07
function stdToTime() {
  let yy = new Date().getFullYear();
  let mm = new Date().getMonth() + 1;
  let dd = new Date().getDate();
  let hh = new Date().getHours();
  let mf = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes();
  let ss = new Date().getSeconds() < 10 ? '0' + new Date().getSeconds() : new Date().getSeconds();
  return yy + '-' + mm + '-' + dd + ' ' + hh + ':' + mf + ':' + ss;
}

export { deepCopyByJson, deepCopyByFor, deepCopyByMap, stdToTime };
