import axios from 'axios';

// 声明一个 Map 用于存储每个请求的标识 和 取消函数
let pendingMap = new Map();

/**
 * @description 得到该格式的url
 */
export const getPendingUrl = config => [config.method, config.url].join('&');

export class AxiosCanceler {
  /**
   * 向pendingMap中增加请求
   * @param {Object} config
   */
  addPending(config) {
    // 请求开始前，检查一下是否已经有该请求了，有则取消掉该请求
    this.removePending(config);
    const url = getPendingUrl(config);
    config.cancelToken =
      config.cancelToken ||
      new axios.CancelToken(cancel => {
        // 如果 pending 中不存在当前请求，则添加进去
        if (!pendingMap.has(url)) {
          pendingMap.set(url, cancel);
        }
      });
  }

  /**
   * 移除请求
   * @param {Object} config
   */
  removePending(config) {
    const url = getPendingUrl(config);
    // 如果在 pending 中存在当前请求标识，需要取消当前请求，并且移除
    if (pendingMap.has(url)) {
      const cancel = pendingMap.get(url);
      cancel(url);
      pendingMap.delete(url);
    }
  }

  /**
   * @description: 清空 pending 中的请求（在路由跳转时调用）
   */
  removeAllPending() {
    pendingMap.forEach(cancel => {
      cancel && typeof cancel === 'function' && cancel();
    });
    pendingMap.clear();
  }

  /**
   * @description: 重置pendingMap
   */
  reset() {
    pendingMap = new Map();
  }
}
