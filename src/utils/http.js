import axios from 'axios';
import { Message } from 'element-ui';
import { AxiosCanceler } from './cancel.js';

const axiosCanceler = new AxiosCanceler();

const http = axios.create({
  baseURL: `${process.env.API_HOST}/api`, // 本地 url + /api
  timeout: 7000, // 超时 7s
});

// 设置请求拦截
// 新的常量 axios http.拦截器.请求.使用 => 有两个参数 一个成功的回调函数  一个失败的回调函数
http.interceptors.request.use(
  (config) => {
    /**
     * 全局拦截请求发送前提交的参数
     * 以下代码为示例，在登录状态下，分别对 post 和 get 请求加上 token 参数
     */
    // 设置请求验证所用token
    const token = localStorage.getItem('l-mall-identity-client');
    if (token) {
      config.headers['l-mall-identity'] = token;
    }

    // 将请求加入pendingMap 已检查是否有该请求
    axiosCanceler.addPending(config);

    // 设置json
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
    // get请求映射params参数
    if (config.method === 'get' && config.params) {
      let url = config.url + '?';
      for (const propName of Object.keys(config.params)) {
        const value = config.params[propName];
        var part = encodeURIComponent(propName) + '=';
        if (value !== null && typeof value !== 'undefined') {
          if (typeof value === 'object') {
            for (const key of Object.keys(value)) {
              let params = propName + '[' + key + ']';
              var subPart = encodeURIComponent(params) + '=';
              url += subPart + encodeURIComponent(value[key]) + '&';
            }
          } else {
            url += part + encodeURIComponent(value) + '&';
          }
        }
      }
      url = url.slice(0, -1);
      config.params = {};
      config.url = url;
    }
    // console.log('config    ', config);
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

// 响应拦截器
http.interceptors.response.use(
  (response) => {
    // 接口响应之后把这次请求清除
    axiosCanceler.removePending(response.config);

    /**
     * 全局拦截请求发送后返回的数据，如果数据有报错则在这做全局的错误提示
     * 假设返回数据格式为：{ code: 1, msg: '', data: {} || [] }
     * 规则是当 code 为 0 时表示请求成功，不为 0 时表示 出错 或 （接口需要登录 登录状态失效，需要重新登录）
     * 请求出错时 error 会返回错误信息
     */
    // 后端 返回错误
    if (response.data.code !== 0) {
      // 提示后端返回的错误信息
      Message({
        message: response.data.msg,
        type: 'error',
      });
    }
    return Promise.resolve(response);
  },
  (error) => {
    return Promise.reject(error);
  },
);

export default http;
