# Vue 2 + Webpack3

# 安装依赖

# npm install

# 运行项目

# npm run dev

```javascript
|-- node_modules                           // 包依赖
|-- config
    |-- index.js                           // 配置信息
    |-- dev.env.js                         // 开发环境变量
    |-- prod.env.js                        // 生产环境变量
|-- src
    |-- api                                // 接口目录
        |-- user.js                        // 用户接口
    |-- assets                             // 静态文件资源
    |-- components
        |-- header.vue                     // 顶部
        |-- right-aside.vue                // 右侧组件---一键置顶
        |-- register.vue                   // 注册组件
    |-- layout
        |-- index.vue                      // 布局
    |-- router
        |-- modules
            |-- home.js                    // 首页相关路由
            |-- user.js                    // 登录路由
        |-- index.js                       // 根路由(导航守卫)
    |-- vuex
        |-- store.js                       // 状态管理vuex
    |-- utils
        |-- publicFunction.js              // 公用方法
        |-- http.js                        // 请求封装
        |-- cancel.js                      // 取消请求封装
    |-- views
        |-- home.vue                       // 首页
        |-- login.vue                      // 登录
    |-- App.vue                            // 根组件
    |-- main.js                            // 主入口
```
